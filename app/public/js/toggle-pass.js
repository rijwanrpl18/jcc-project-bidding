$(document).ready(function(){
    $('#togglePass').click(function(){
        var passwordField  = $('#password');
        var passwordFieldType = passwordField.attr('type');
        var icon = $('#icon-pass');
        if(passwordField.val() != ''){
            if (passwordFieldType == 'password') {
                passwordField.attr('type', 'text');
                icon.removeClass('fa-eye');
                icon.addClass('fa-eye-slash');
                $(this).attr('title', 'Hide Password');
            }else{
                passwordField.attr('type', 'password');
                icon.removeClass('fa-eye-slash');
                icon.addClass('fa-eye');
                $(this).attr('title', 'Show Password');
            }
        }else{
            alert('Masukan Password');
        }
    });
});