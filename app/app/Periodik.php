<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodik extends Model
{
    protected $table = 'periodik';
    protected $guarded = ['id'];
}
