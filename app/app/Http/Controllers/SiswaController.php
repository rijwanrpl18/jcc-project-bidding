<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use App\Village;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function __construct(){
        $this->middleware(['auth', 'verified']);
    }

    public function index(Request $request)
    {
        if (auth()->user()->role == 'siswa') {
            return redirect()->route('home');
        }

        $data['q'] = $_GET['q'] ?? '';
        $data['limit'] = $_GET['limit'] ?? 10;

        $data['data'] = Profile::paginate($data['limit']);
        $data['active_menu'] = 'siswa';
        $data['breadcrumb'] = [
            'Data Siswa' => route('siswa')
        ];

        return view('siswa', compact('data'));
    }

    public function show(Request $request, $id)
    {
        $data['data'] = User::findOrFail($id);
        $data['active_menu'] = 'siswa';
        $data['master'] = config('base');
        $data['desa'] = Village::with(['district', 'regency', 'province'])->find($data['data']->profile->kelurahan_id);
        $data['breadcrumb'] = [
            'Data Siswa' => route('siswa'),
            $data['data']->nama => route('siswa.show', ['id', $id])
        ];
        return view('detail', compact('data'));
    }
}
