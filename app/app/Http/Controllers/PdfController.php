<?php

namespace App\Http\Controllers;

use App\User;
use App\Village;
use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;

class PdfController extends Controller
{
    public function __construct(){
        $this->middleware(['auth', 'verified']);
    }

    public function index(Request $request, $id)
    {
        if(auth()->user()->role == 'siswa'){
            $id = auth()->user()->id;
        }
        
        $data['data'] = User::findOrFail($id);
        $data['active_menu'] = 'siswa';
        $data['master'] = config('base');
        $data['desa'] = Village::with(['district', 'regency', 'province'])->find($data['data']->profile->kelurahan_id);
        // return view('download', compact('data'));

        $pdf = PDF::loadView('download', compact('data'));
        // $pdf->getDomPDF()->setHttpContext(
        //     stream_context_create([
        //         'ssl' => [
        //             'allow_self_signed'=> TRUE,
        //             'verify_peer' => FALSE,
        //             'verify_peer_name' => FALSE,
        //         ]
        //     ])
        // );
        return $pdf->download(Carbon::now()->format('YmdHis') . '-'.$data['data']->nama.'.pdf');
    }
}
