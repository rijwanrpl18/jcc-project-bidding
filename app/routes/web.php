<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);

Route::get('confirm', 'Auth\ConfirmController@index')->name('confirm');

Route::get('logout', 'Auth\LoginController@logout');

Route::get('/desa', 'HomeController@getVillage');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/profile', 'HomeController@update')->name('profile.update');
Route::get('/siswa', 'SiswaController@index')->name('siswa');
Route::get('/siswa/{id}', 'SiswaController@show')->name('siswa.show');

Route::get('/download-pdf/{id}', 'PdfController@index')->name('download.pdf');

// Route::get('testmail', 'Auth\RegisterController@sendMail');

