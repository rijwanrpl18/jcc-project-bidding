@extends('layouts.application')
@section('module', 'Edit Profile')

@section('content')
@if (auth()->user()->role == 'siswa')
    <form 
        action="{{ route('profile.update') }}" 
        method="POST" 
        enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-header h4">
                        Data Siswa
                    </div>
                    <div class="card-body">
                        {{-- Nama --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Nama *</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nama')?' is-invalid':'' }}" 
                                    placeholder="Nama"
                                    name="nama" 
                                    id="nama" 
                                    value="{{ old('nama') ?? $data['data']->nama }}" 
                                    required>
                                {!! $errors->first('nama', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Kategori Pendaftar --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Kategori Pendaftar *</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('kategori')?' is-invalid':'' }}" 
                                    required
                                    name="kategori">
                                    @foreach ($data['master']['kategori_pendaftar'] as $key => $value)
                                        <option 
                                            value="{{$key}}" 
                                            {{ $data['data']->profile != null && $data['data']->profile->kategori == $key ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('kategori', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Program --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Program *</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('program')?' is-invalid':'' }}" 
                                    required
                                    name="program">
                                    @foreach ($data['master']['program'] as $key => $value)
                                        <option 
                                            value="{{$key}}" 
                                            {{ $data['data']->profile != null && $data['data']->profile->program == $key ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('program', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- NISN --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">NISN *</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nisn')?' is-invalid':'' }}" 
                                    placeholder="NISN"
                                    name="nisn" 
                                    id="nisn" 
                                    value="{{ old('nisn') ?? ($data['data']->profile != null ? $data['data']->profile->nisn : '') }}" 
                                    required>
                                {!! $errors->first('nisn', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Jenis Kelamin --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Jenis Kelamin *</label>
                            <div class="col-md-9">
                                <label class="form-check form-check-inline">
                                    <input 
                                        class="form-check-input" 
                                        type="radio" 
                                        name="jk" 
                                        value="0" 
                                        {{ $data['data']->profile != null && $data['data']->profile->jk == 0 ? 'checked' : ''}}>
                                    <span class="form-check-label">Perempuan</span>
                                </label>
                                <label class="form-check form-check-inline">
                                    <input 
                                        class="form-check-input" 
                                        type="radio" 
                                        name="jk" 
                                        value="1" 
                                        {{ $data['data']->profile != null && $data['data']->profile->jk == 1 ? 'checked' : ''}}>
                                    <span class="form-check-label">Laki - Laki</span>
                                </label>
                            </div>
                        </div>

                        {{-- Asal Sekolah --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Asal Sekolah *</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('asal_sekolah')?' is-invalid':'' }}" 
                                    placeholder="Asal Sekolah"
                                    name="asal_sekolah" 
                                    id="asal_sekolah" 
                                    value="{{ old('asal_sekolah') ?? ($data['data']->profile != null ? $data['data']->profile->asal_sekolah : '') }}" 
                                    required>
                                {!! $errors->first('asal_sekolah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- NIK --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">NIK</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nik')?' is-invalid':'' }}" 
                                    placeholder="NIK"
                                    name="nik" 
                                    id="nik" 
                                    value="{{ old('nik') ?? ($data['data']->profile != null ? $data['data']->profile->nik : '') }}" >
                                {!! $errors->first('nik', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Tempat Lahir --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Tempat Lahir</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('tempat_lahir')?' is-invalid':'' }}" 
                                    placeholder="Tempat Lahir"
                                    name="tempat_lahir" 
                                    id="tempat_lahir" 
                                    value="{{ old('tempat_lahir') ?? ($data['data']->profile != null ? $data['data']->profile->tempat_lahir : '') }}" >
                                {!! $errors->first('tempat_lahir', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Tanggal Lahir --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('tanggal_lahir')?' is-invalid':'' }} datepicker" 
                                    placeholder="Tanggal Lahir" 
                                    name="tanggal_lahir" 
                                    id="tanggal_lahir" 
                                    value="{{ old('tanggal_lahir') ?? ($data['data']->profile != null ? $data['data']->profile->tanggal_lahir : '') }}">
                                {!! $errors->first('tanggal_lahir', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Agama --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Agama</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('agama')?' is-invalid':'' }}" 
                                    name="agama">
                                    @foreach ($data['master']['agama'] as $key => $value)
                                        <option 
                                            value="{{$key}}" 
                                            {{ $data['data']->profile != null && $data['data']->profile->agama == $key ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('agama', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Berkebutuhan Khusus --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Berkebutuhan Khusus</label>
                            <div class="col-md-9">
                                <label class="form-check form-check-inline">
                                    <input 
                                        class="form-check-input" 
                                        type="radio" 
                                        name="kebutuhan_khusus" 
                                        value="0" 
                                        {{ $data['data']->profile != null && $data['data']->profile->kebutuhan_khusus == 0 ? 'checked' : ''}}>
                                    <span class="form-check-label">Tidak</span>
                                </label>
                                <label class="form-check form-check-inline">
                                    <input 
                                        class="form-check-input" 
                                        type="radio" 
                                        name="kebutuhan_khusus" 
                                        value="1" 
                                        {{ $data['data']->profile != null && $data['data']->profile->kebutuhan_khusus == 1 ? 'checked' : ''}}>
                                    <span class="form-check-label">Ya</span>
                                </label>
                            </div>
                        </div>

                        {{-- Alamat --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Alamat *</label>
                            <div class="col-md-9">
                                <textarea 
                                    name="alamat" 
                                    id="alamat" 
                                    cols="30" 
                                    rows="5" 
                                    required
                                    class="form-control {{ $errors->has('alamat')?' is-invalid':'' }}">{{ old('alamat') ?? ($data['data']->profile != null ? $data['data']->profile->alamat : '') }}</textarea>
                                {!! $errors->first('alamat', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- RT --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">RT</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('rt')?' is-invalid':'' }}" 
                                    placeholder="RT"
                                    name="rt" 
                                    id="rt" 
                                    value="{{ old('rt') ?? ($data['data']->profile != null ? $data['data']->profile->rt : '') }}" >
                                {!! $errors->first('rt', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- RW --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">RW</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('rw')?' is-invalid':'' }}" 
                                    placeholder="RW"
                                    name="rw" 
                                    id="rw" 
                                    value="{{ old('rw') ?? ($data['data']->profile != null ? $data['data']->profile->rw : '') }}" >
                                {!! $errors->first('rw', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Desa --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Kelurahan / Desa *</label>
                            <div class="col-md-9">
                                <select 
                                    name="kelurahan_id" 
                                    class="form-control desa {{ $errors->has('kelurahan_id')?' has-danger':'' }}">
                                    @if ($data['data']->profile != null && isset($data['data']->profile->kelurahan_id))
                                        <option 
                                            value="{{ $data['data']->profile->kelurahan_id }}" 
                                            selected>
                                            {{ $data['desa']->name }}, {{ $data['desa']->district->name }}, {{ $data['desa']->regency->name }}, {{ $data['desa']->province->name }}
                                        </option>
                                    @endif
                                </select>
                                <small class="form-text text-muted">Ketikan nama desa.</small>
                                {!! $errors->first('kelurahan_id', '<small class="text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Kode Pos --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Kode Pos</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('kode_pos')?' is-invalid':'' }}" 
                                    placeholder="Kode Pos"
                                    name="kode_pos" 
                                    id="kode_pos" 
                                    value="{{ old('kode_pos') ?? ($data['data']->profile != null ? $data['data']->profile->kode_pos : '') }}" >
                                {!! $errors->first('kode_pos', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Alat Transportasi ke Sekolah --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Alat Transportasi ke Sekolah</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('alat_transportasi')?' is-invalid':'' }}" 
                                    name="alat_transportasi">
                                    @foreach ($data['master']['alat_transportasi'] as $key => $value)
                                        <option 
                                            value="{{$key}}" 
                                            {{ $data['data']->profile != null && $data['data']->profile->alat_transportasi == $key ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('alat_transportasi', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Jenis Tinggal --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Jenis Tinggal</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('jenis_tinggal')?' is-invalid':'' }}" 
                                    name="jenis_tinggal">
                                    @foreach ($data['master']['jenis_tinggal'] as $key => $value)
                                        <option 
                                            value="{{$key}}" 
                                            {{ $data['data']->profile != null && $data['data']->profile->jenis_tinggal == $key ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('jenis_tinggal', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- No Telepon Rumah --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">No Telepon Rumah</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('telepon_rumah')?' is-invalid':'' }}" 
                                    placeholder="No Telepon Rumah"
                                    name="telepon_rumah" 
                                    id="telepon_rumah" 
                                    value="{{ old('telepon_rumah') ?? ($data['data']->profile != null ? $data['data']->profile->telepon_rumah : '') }}" >
                                {!! $errors->first('telepon_rumah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- No Telepon --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">No Telepon *</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    required
                                    class="form-control {{ $errors->has('telepon')?' is-invalid':'' }}" 
                                    placeholder="No Telepon"
                                    name="telepon" 
                                    id="telepon" 
                                    value="{{ old('telepon') ?? ($data['data']->profile != null ? $data['data']->profile->telepon : '') }}" >
                                {!! $errors->first('telepon', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Email --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Email *</label>
                            <div class="col-md-9">
                                <input 
                                    type="email" 
                                    required
                                    class="form-control {{ $errors->has('email')?' is-invalid':'' }}" 
                                    placeholder="Email"
                                    name="email" 
                                    id="email" 
                                    value="{{ old('email') ?? $data['data']->email }}" >
                                {!! $errors->first('email', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Password --}}
                        <div 
                            class="form-group row" 
                            style="position: relative;">
                            <label class="form-label col-md-3">
                                <div>Password</div>
                            </label>
                            <div class="col-md-9">
                                <input 
                                    type="password" 
                                    class="form-control" 
                                    name="password" 
                                    placeholder="password" 
                                    id="password">
                                <div 
                                    style="position: absolute; right: 10px; top: 25%; transform: translate(-50%,0); cursor: pointer;" 
                                    id="togglePass">
                                    <i class="fa fa-eye" id="icon-pass"></i>
                                </div>
                                <small class="text-muted">Kosongkan password jika tidak mengubah password.</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            {{-- Ayah --}}
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header h4">
                        Data Ayah
                    </div>
                    <div class="card-body">
                        {{-- Nama --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Nama *</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nama_ayah')?' is-invalid':'' }}" 
                                    placeholder="Nama"
                                    name="nama_ayah" 
                                    id="nama_ayah" 
                                    value="{{ old('nama_ayah') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->nama_ayah : '') }}" 
                                    required>
                                {!! $errors->first('nama_ayah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- NIK --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">NIK</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nik_ayah')?' is-invalid':'' }}" 
                                    placeholder="NIK"
                                    name="nik_ayah" 
                                    id="nik_ayah" 
                                    value="{{ old('nik_ayah') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->nik_ayah : '') }}">
                                {!! $errors->first('nik_ayah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Tahun Lahir --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Tahun Lahir</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('tahun_lahir_ayah')?' is-invalid':'' }}" 
                                    placeholder="Tahun Lahir"
                                    name="tahun_lahir_ayah" 
                                    id="tahun_lahir_ayah" 
                                    value="{{ old('tahun_lahir_ayah') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->tahun_lahir_ayah : '') }}">
                                {!! $errors->first('tahun_lahir_ayah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Pendidikan --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Jenjang Pendidikan</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('pendidikan_ayah')?' is-invalid':'' }}"
                                    name="pendidikan_ayah">
                                    @foreach ($data['master']['pendidikan'] as $key => $value)
                                        <option 
                                            value="{{ $value }}" 
                                            {{ $data['data']->orangtua != null && $data['data']->orangtua->pendidikan_ayah == $value ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('pendidikan_ayah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Pekerjaan --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Pekerjaan</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('pekerjaan_ayah')?' is-invalid':'' }}"
                                    name="pekerjaan_ayah">
                                    @foreach ($data['master']['pekerjaan'] as $key => $value)
                                        <option 
                                            value="{{ $value }}" 
                                            {{ $data['data']->orangtua != null && $data['data']->orangtua->pekerjaan_ayah == $value ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('pekerjaan_ayah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Penghasilan --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Penghasilan</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('penghasilan_ayah')?' is-invalid':'' }}"
                                    name="penghasilan_ayah">
                                    @foreach ($data['master']['penghasilan'] as $key => $value)
                                        <option 
                                            value="{{ $value }}" 
                                            {{ $data['data']->orangtua != null && $data['data']->orangtua->penghasilan_ayah == $value ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('penghasilan_ayah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- No HP/WA --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">No HP/WA</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('telepon_ayah')?' is-invalid':'' }}" 
                                    placeholder="No HP/WA"
                                    name="telepon_ayah" 
                                    id="telepon_ayah" 
                                    value="{{ old('telepon_ayah') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->telepon_ayah : '') }}">
                                {!! $errors->first('telepon_ayah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Ibu --}}
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header h4">
                        Data Ibu
                    </div>
                    <div class="card-body">
                        {{-- Nama --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Nama *</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nama_ibu')?' is-invalid':'' }}" 
                                    placeholder="Nama"
                                    name="nama_ibu" 
                                    id="nama_ibu" 
                                    value="{{ old('nama_ibu') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->nama_ibu : '') }}" 
                                    required>
                                {!! $errors->first('nama_ibu', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- NIK --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">NIK</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nik_ibu')?' is-invalid':'' }}" 
                                    placeholder="NIK"
                                    name="nik_ibu" 
                                    id="nik_ibu" 
                                    value="{{ old('nik_ibu') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->nik_ibu : '') }}">
                                {!! $errors->first('nik_ibu', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Tahun Lahir --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Tahun Lahir</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('tahun_lahir_ibu')?' is-invalid':'' }}" 
                                    placeholder="Tahun Lahir"
                                    name="tahun_lahir_ibu" 
                                    id="tahun_lahir_ibu" 
                                    value="{{ old('tahun_lahir_ibu') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->tahun_lahir_ibu : '') }}">
                                {!! $errors->first('tahun_lahir_ibu', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Pendidikan --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Jenjang Pendidikan</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('pendidikan_ibu')?' is-invalid':'' }}"
                                    name="pendidikan_ibu">
                                    @foreach ($data['master']['pendidikan'] as $key => $value)
                                        <option 
                                            value="{{ $value }}" 
                                            {{ $data['data']->orangtua != null && $data['data']->orangtua->pendidikan_ibu == $value ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('pendidikan_ibu', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Pekerjaan --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Pekerjaan</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('pekerjaan_ibu')?' is-invalid':'' }}"
                                    name="pekerjaan_ibu">
                                    @foreach ($data['master']['pekerjaan'] as $key => $value)
                                        <option 
                                            value="{{ $value }}" 
                                            {{ $data['data']->orangtua != null && $data['data']->orangtua->pekerjaan_ibu == $value ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('pekerjaan_ibu', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Penghasilan --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Penghasilan</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('penghasilan_ibu')?' is-invalid':'' }}"
                                    name="penghasilan_ibu">
                                    @foreach ($data['master']['penghasilan'] as $key => $value)
                                        <option 
                                            value="{{ $value }}" 
                                            {{ $data['data']->orangtua != null && $data['data']->orangtua->penghasilan_ibu == $value ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('penghasilan_ibu', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- No HP/WA --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">No HP/WA</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('telepon_ibu')?' is-invalid':'' }}" 
                                    placeholder="No HP/WA"
                                    name="telepon_ibu" 
                                    id="telepon_ibu" 
                                    value="{{ old('telepon_ibu') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->telepon_ibu : '') }}">
                                {!! $errors->first('telepon_ibu', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Wali --}}
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header h4">
                        Data Wali
                    </div>
                    <div class="card-body">
                        {{-- Nama --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Nama</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nama_wali')?' is-invalid':'' }}" 
                                    placeholder="Nama"
                                    name="nama_wali" 
                                    id="nama_wali" 
                                    value="{{ old('nama_wali') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->nama_wali : '') }}" >
                                {!! $errors->first('nama_wali', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- NIK --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">NIK</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nik_wali')?' is-invalid':'' }}" 
                                    placeholder="NIK"
                                    name="nik_wali" 
                                    id="nik_wali" 
                                    value="{{ old('nik_wali') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->nik_wali : '') }}">
                                {!! $errors->first('nik_wali', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Tahun Lahir --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Tahun Lahir</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('tahun_lahir_wali')?' is-invalid':'' }}" 
                                    placeholder="Tahun Lahir"
                                    name="tahun_lahir_wali" 
                                    id="tahun_lahir_wali" 
                                    value="{{ old('tahun_lahir_wali') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->tahun_lahir_wali : '') }}">
                                {!! $errors->first('tahun_lahir_wali', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Pendidikan --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Jenjang Pendidikan</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('pendidikan_wali')?' is-invalid':'' }}"
                                    name="pendidikan_wali">
                                    @foreach ($data['master']['pendidikan'] as $key => $value)
                                        <option 
                                            value="{{ $value }}" 
                                            {{ $data['data']->orangtua != null && $data['data']->orangtua->pendidikan_wali == $value ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('pendidikan_wali', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Pekerjaan --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Pekerjaan</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('pekerjaan_wali')?' is-invalid':'' }}"
                                    name="pekerjaan_wali">
                                    @foreach ($data['master']['pekerjaan'] as $key => $value)
                                        <option 
                                            value="{{ $value }}" 
                                            {{ $data['data']->orangtua != null && $data['data']->orangtua->pekerjaan_wali == $value ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('pekerjaan_wali', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                        
                        {{-- Penghasilan --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Penghasilan</label>
                            <div class="col-md-9">
                                <select 
                                    class="form-control select2 {{ $errors->has('penghasilan_wali')?' is-invalid':'' }}"
                                    name="penghasilan_wali">
                                    @foreach ($data['master']['penghasilan'] as $key => $value)
                                        <option 
                                            value="{{ $value }}" 
                                            {{ $data['data']->orangtua != null && $data['data']->orangtua->penghasilan_wali == $value ? 'selected' : '' }}>
                                            {{ $value }}
                                        </option>
                                    @endforeach
                                </select>
                                {!! $errors->first('penghasilan_wali', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- No HP/WA --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">No HP/WA</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('telepon_wali')?' is-invalid':'' }}" 
                                    placeholder="No HP/WA"
                                    name="telepon_wali" 
                                    id="telepon_wali" 
                                    value="{{ old('telepon_wali') ?? ($data['data']->orangtua != null ? $data['data']->orangtua->telepon_wali : '') }}">
                                {!! $errors->first('telepon_wali', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Prestasi --}}
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header h4">
                        Data Prestasi
                    </div>
                    <div class="card-body">
                        {{-- Nama Prestasi --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Nama Prestasi</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('nama_prestasi')?' is-invalid':'' }}" 
                                    placeholder="Nama Prestasi"
                                    name="nama_prestasi" 
                                    id="nama_prestasi" 
                                    value="{{ old('nama_prestasi') ?? ($data['data']->prestasi != null ? $data['data']->prestasi->nama_prestasi : '') }}">
                                {!! $errors->first('nama_prestasi', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Tingkat --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Tingkat</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('tingkat')?' is-invalid':'' }}" 
                                    placeholder="Tingkat"
                                    name="tingkat" 
                                    id="tingkat" 
                                    value="{{ old('tingkat') ?? ($data['data']->prestasi != null ? $data['data']->prestasi->tingkat : '') }}">
                                {!! $errors->first('tingkat', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Juara Ke --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Juara Ke</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('juara')?' is-invalid':'' }}" 
                                    placeholder="Juara Ke"
                                    name="juara" 
                                    id="juara" 
                                    value="{{ old('juara') ?? ($data['data']->prestasi != null ? $data['data']->prestasi->juara : '') }}">
                                {!! $errors->first('juara', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>

                        {{-- Tahun --}}
                        <div class="form-group row">
                            <label class="form-label col-md-3">Tahun</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('tahun')?' is-invalid':'' }}" 
                                    placeholder="Tahun"
                                    name="tahun" 
                                    id="tahun" 
                                    value="{{ old('tahun') ?? ($data['data']->prestasi != null ? $data['data']->prestasi->tahun : '') }}">
                                {!! $errors->first('tahun', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Periodik --}}
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header h4">
                        Data Periodik
                    </div>
                    <div class="card-body">
                        {{-- Tinggi Badan --}}
                        <div class="form-group">
                            <label class="form-label">Tinggi Badan</label>
                            <div class="input-group">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('tinggi_badan')?' is-invalid':'' }}" 
                                    placeholder="Tinggi Badan"
                                    name="tinggi_badan" 
                                    id="tinggi_badan" 
                                    value="{{ old('tinggi_badan') ?? ($data['data']->periodik != null ? $data['data']->periodik->tinggi_badan : '') }}" 
                                    required>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">cm</span>
                                </div>
                            </div>
                            {!! $errors->first('tinggi_badan', '<small class="form-text text-danger">:message</small>') !!}
                        </div>

                        {{-- Berat Badan --}}
                        <div class="form-group">
                            <label class="form-label">Berat Badan</label>
                            <div class="input-group">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('berat_badan')?' is-invalid':'' }}" 
                                    placeholder="Berat Badan"
                                    name="berat_badan" 
                                    id="berat_badan" 
                                    value="{{ old('berat_badan') ?? ($data['data']->periodik != null ? $data['data']->periodik->berat_badan : '') }}" 
                                    required>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Kg</span>
                                </div>
                            </div>
                            {!! $errors->first('berat_badan', '<small class="form-text text-danger">:message</small>') !!}
                        </div>

                        {{-- Lingkar Kepala --}}
                        <div class="form-group">
                            <label class="form-label">Lingkar Kepala</label>
                            <div class="input-group">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('lingkar_kepala')?' is-invalid':'' }}" 
                                    placeholder="Lingkar Kepala"
                                    name="lingkar_kepala" 
                                    id="lingkar_kepala" 
                                    value="{{ old('lingkar_kepala') ?? ($data['data']->periodik != null ? $data['data']->periodik->lingkar_kepala : '') }}" 
                                    required>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">cm</span>
                                </div>
                            </div>
                            {!! $errors->first('lingkar_kepala', '<small class="form-text text-danger">:message</small>') !!}
                        </div>

                        {{-- Jarak Tempat Tinggal ke Sekolah --}}
                        <div class="form-group">
                            <label class="form-label">Jarak Tempat Tinggal ke Sekolah</label>
                            <div class="input-group">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('jarak')?' is-invalid':'' }}" 
                                    placeholder="Jarak Tempat Tinggal ke Sekolah"
                                    name="jarak" 
                                    id="jarak" 
                                    value="{{ old('jarak') ?? ($data['data']->periodik != null ? $data['data']->periodik->jarak : '') }}" 
                                    required>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">meter</span>
                                </div>
                            </div>
                            {!! $errors->first('jarak', '<small class="form-text text-danger">:message</small>') !!}
                        </div>

                        {{-- Waktu Tempuh Berangkat ke Sekolah --}}
                        <div class="form-group">
                            <label class="form-label ">Waktu Tempuh Berangkat ke Sekolah</label>
                            <div class="input-group ">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('waktu_tempuh')?' is-invalid':'' }}" 
                                    placeholder="Waktu Tempuh Berangkat ke Sekolah"
                                    name="waktu_tempuh" 
                                    id="waktu_tempuh" 
                                    value="{{ old('waktu_tempuh') ?? ($data['data']->periodik != null ? $data['data']->periodik->waktu_tempuh : '') }}" 
                                    required>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">menit</span>
                                </div>
                            </div>
                            {!! $errors->first('waktu_tempuh', '<small class="form-text text-danger">:message</small>') !!}
                        </div>

                        {{-- Jumlah Saudara Kandung --}}
                        <div class="form-group">
                            <label class="form-label">Jumlah Saudara Kandung</label>
                            <div class="input-group">
                                <input 
                                    type="text" 
                                    class="form-control {{ $errors->has('jumlah_saudara')?' is-invalid':'' }}" 
                                    placeholder="Jumlah Saudara Kandung"
                                    name="jumlah_saudara" 
                                    id="jumlah_saudara" 
                                    value="{{ old('jumlah_saudara') ?? ($data['data']->periodik != null ? $data['data']->periodik->jumlah_saudara : '') }}" 
                                    required>
                                <div class="input-group-prepend">
                                    <span class="input-group-text">orang</span>
                                </div>
                            </div>
                            {!! $errors->first('jumlah_saudara', '<small class="form-text text-danger">:message</small>') !!}
                        </div>
                    </div>
                </div>
            </div>
            {{-- Lampiran --}}
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header h4">
                        Lampiran
                    </div>
                    <div class="card-body">
                        {{-- Akte Kelahiran --}}
                        <div class="form-group">
                            <label class="form-label">Akte Kelahiran</label>
                            <div class="input-group file-input">
                                <label class="custom-file">
                                    <input 
                                        type="file" 
                                        class="custom-file-input upload {{ $errors->has('akte')?' is-invalid':'' }}" 
                                        data-target="akte" 
                                        name="akte" 
                                        placeholder="No File Selected">
                                    <span class="custom-file-label" id="akte"></span>
                                </label>
                            </div>
                            {!! $errors->first('akte', '<small class="form-text text-danger">:message</small>') !!}
                            <small class="form-text text-muted">File harus berformat .jpg/.jpeg/.png/.pdf</small>
                            @if ($data['data']->lampiran != null && $data['data']->lampiran->akte)
                                File sudah ada, <a href="{{ asset('storage/'.$data['data']->lampiran->akte) }}" target="_blank">Lihat disini</a>
                            @endif
                        </div>

                        {{-- Kartu Keluarga --}}
                        <div class="form-group">
                            <label class="form-label">Kartu Keluarga</label>
                            <div class="input-group file-input">
                                <label class="custom-file">
                                    <input 
                                        type="file" 
                                        class="custom-file-input upload {{ $errors->has('kk')?' is-invalid':'' }}" 
                                        data-target="kk" 
                                        name="kk" 
                                        placeholder="No File Selected">
                                    <span class="custom-file-label" id="kk"></span>
                                </label>
                            </div>
                            {!! $errors->first('kk', '<small class="form-text text-danger">:message</small>') !!}
                            <small class="form-text text-muted">File harus berformat .jpg/.jpeg/.png/.pdf</small>
                            @if ($data['data']->lampiran != null && $data['data']->lampiran->kk)
                                File sudah ada, <a href="{{ asset('storage/'.$data['data']->lampiran->kk) }}" target="_blank">Lihat disini</a>
                            @endif
                        </div>

                        {{-- Badge (PTB, Mitra PTB dan Vidarta) --}}
                        <div class="form-group">
                            <label class="form-label">Badge (PTB, Mitra PTB dan Vidarta)</label>
                            <div class="input-group file-input">
                                <label class="custom-file">
                                    <input 
                                        type="file" 
                                        class="custom-file-input upload {{ $errors->has('badge')?' is-invalid':'' }}" 
                                        data-target="badge" 
                                        name="badge" 
                                        placeholder="No File Selected">
                                    <span class="custom-file-label" id="badge"></span>
                                </label>
                            </div>
                            {!! $errors->first('badge', '<small class="form-text text-danger">:message</small>') !!}
                            <small class="form-text text-muted">File harus berformat .jpg/.jpeg/.png/.pdf</small>
                            @if ($data['data']->lampiran != null && $data['data']->lampiran->badge)
                                File sudah ada, <a href="{{ asset('storage/'.$data['data']->lampiran->badge) }}" target="_blank">Lihat disini</a>
                            @endif
                        </div>

                        {{-- Pas Foto berwarna (terbaru) --}}
                        <div class="form-group">
                            <label class="form-label">Pas Foto berwarna (terbaru)</label>
                            <div class="input-group file-input">
                                <label class="custom-file">
                                    <input 
                                        type="file" 
                                        class="custom-file-input upload {{ $errors->has('foto')?' is-invalid':'' }}" 
                                        data-target="foto" 
                                        name="foto" 
                                        placeholder="No File Selected">
                                    <span class="custom-file-label" id="foto"></span>
                                </label>
                            </div>
                            {!! $errors->first('foto', '<small class="form-text text-danger">:message</small>') !!}
                            <small class="form-text text-muted">File harus berformat .jpg/.jpeg/.png</small>
                            @if ($data['data']->lampiran != null && $data['data']->lampiran->foto)
                                File sudah ada, <a href="{{ asset('storage/'.$data['data']->lampiran->foto) }}" target="_blank">Lihat disini</a>
                            @endif
                        </div>

                        {{-- Surat Keterangan Tanda Lulus SD (SKL) --}}
                        <div class="form-group">
                            <label class="form-label">Surat Keterangan Tanda Lulus SD (SKL)</label>
                            <div class="input-group file-input">
                                <label class="custom-file">
                                    <input 
                                        type="file" 
                                        class="custom-file-input upload {{ $errors->has('skl')?' is-invalid':'' }}" 
                                        data-target="skl" 
                                        name="skl" 
                                        placeholder="No File Selected">
                                    <span class="custom-file-label" id="skl"></span>
                                </label>
                            </div>
                            {!! $errors->first('skl', '<small class="form-text text-danger">:message</small>') !!}
                            <small class="form-text text-muted">File harus berformat .jpg/.jpeg/.png/.pdf</small>
                            @if ($data['data']->lampiran != null && $data['data']->lampiran->skl)
                                File sudah ada, <a href="{{ asset('storage/'.$data['data']->lampiran->skl) }}" target="_blank">Lihat disini</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-success">Simpan</button>
        </div>
    </form>
@else
    <form 
        action="{{ route('profile.update') }}" 
        method="POST" 
        enctype="multipart/form-data">
        @csrf
    </form>
@endif
@endsection

@section('scripts')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="{{ asset('js/toggle-pass.js') }}"></script>
    <script src="{{ asset('js/file-upload.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.desa').select2({
                placeholder: 'Pilih Kelurahan/Desa',
                ajax: {
                    url: '/desa',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                    text: item.name+', '+item.district_name+', '+item.regency_name+', '+item.province_name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@endsection
