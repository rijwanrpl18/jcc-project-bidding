@extends('layouts.application')
@section('module', 'Data Siswa')

@section('content')
    <div class="row">
        <div class="col-md-12 mb-3">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h4>Detail Siswa</h4>
                    <a 
                        href="{{ route('download.pdf', ['id' => $data['data']->id]) }}"
                        class="btn btn-success">
                        <i class="fa fa-download"></i> Download
                    </a>
                </div>
                <div class="card-body pt-0">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img 
                                src="{{ $data['data']->lampiran->foto ? asset('storage/'.$data['data']->lampiran->foto) : asset('assets/img/profile.png') }}" 
                                class="mt-3" 
                                style="width: 300px !important; height: auto;"
                                />
                        </div>
                        <div class="col-md-8">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>Kategori Pendaftar</td>
                                        <td>: {{ $data['master']['kategori_pendaftar'][$data['data']->profile->kategori] ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td>: {{ $data['data']->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>NISN</td>
                                        <td>: {{ $data['data']->profile->nisn }}</td>
                                    </tr>
                                    <tr>
                                        <td>Program</td>
                                        <td>: {{ $data['master']['program'][$data['data']->profile->program] ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Kelamin</td>
                                        <td>: {{ $data['data']->profile->jk ? 'Laki-Laki' : 'Perempuan' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Asal Sekolah</td>
                                        <td>: {{ $data['data']->profile->asal_sekolah }}</td>
                                    </tr>
                                    <tr>
                                        <td>NIK</td>
                                        <td>: {{ $data['data']->profile->nik }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tempat Lahir</td>
                                        <td>: {{ $data['data']->profile->tempat_lahir }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Lahir</td>
                                        <td>: {{ $data['data']->profile->tanggal_lahir }}</td>
                                    </tr>
                                    <tr>
                                        <td>Agama</td>
                                        <td>: {{ $data['master']['agama'][$data['data']->profile->agama] ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Berkebutuhan Khusus</td>
                                        <td>: {{ $data['data']->profile->kebutuhan_khusus ? 'Ya' : 'Tidak' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>: {{ $data['data']->profile->alamat }}</td>
                                    </tr>
                                    <tr>
                                        <td>RT</td>
                                        <td>: {{ $data['data']->profile->rt }}</td>
                                    </tr>
                                    <tr>
                                        <td>RW</td>
                                        <td>: {{ $data['data']->profile->rw }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kelurahan/Desa</td>
                                        <td>: {{ $data['desa']->name ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>: {{ $data['desa']->district->name ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kabupaten/Kota</td>
                                        <td>: {{ $data['desa']->regency->name ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Provinsi</td>
                                        <td>: {{ $data['desa']->province->name ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Kode Pos</td>
                                        <td>: {{ $data['data']->profile->kode_pos }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alat Transportasi ke Sekolah</td>
                                        <td>: {{ $data['master']['alat_transportasi'][$data['data']->profile->alat_transportasi] ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Tinggal</td>
                                        <td>: {{ $data['master']['jenis_tinggal'][$data['data']->profile->jenis_tinggal] ?? '' }}</td>
                                    </tr>
                                    <tr>
                                        <td>No Telepon Rumah</td>
                                        <td>: {{ $data['data']->profile->telepon_rumah }}</td>
                                    </tr>
                                    <tr>
                                        <td>No HP/WA Siswa</td>
                                        <td>: {{ $data['data']->profile->telepon }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>: {{ $data['data']->email }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        {{-- ayah --}}
        <div class="col-md-6 mb-3">
            <div class="card">
                <div class="card-header h4">
                    Data Ayah
                </div>
                <div class="card-body pt-0">
                    <table>
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td>: {{ $data['data']->orangtua->nama_ayah }}</td>
                            </tr>
                            <tr>
                                <td>NIK</td>
                                <td>: {{ $data['data']->orangtua->nik_ayah }}</td>
                            </tr>
                            <tr>
                                <td>Tahun Lahir</td>
                                <td>: {{ $data['data']->orangtua->tahun_lahir_ayah }}</td>
                            </tr>
                            <tr>
                                <td>Jenjang Pendidikan</td>
                                <td>: {{ $data['data']->orangtua->pendidikan_ayah }}</td>
                            </tr>
                            <tr>
                                <td>Pekerjaan</td>
                                <td>: {{ $data['data']->orangtua->pekerjaan_ayah }}</td>
                            </tr>
                            <tr>
                                <td>Pengahasilan Bulanan</td>
                                <td>: {{ $data['data']->orangtua->penghasilan_ayah }}</td>
                            </tr>
                            <tr>
                                <td>No HP/WA</td>
                                <td>: {{ $data['data']->orangtua->telepon_ayah }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- ibu --}}
        <div class="col-md-6 mb-3">
            <div class="card">
                <div class="card-header h4">
                    Data Ibu
                </div>
                <div class="card-body pt-0">
                    <table>
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td>: {{ $data['data']->orangtua->nama_ibu }}</td>
                            </tr>
                            <tr>
                                <td>NIK</td>
                                <td>: {{ $data['data']->orangtua->nik_ibu }}</td>
                            </tr>
                            <tr>
                                <td>Tahun Lahir</td>
                                <td>: {{ $data['data']->orangtua->tahun_lahir_ibu }}</td>
                            </tr>
                            <tr>
                                <td>Jenjang Pendidikan</td>
                                <td>: {{ $data['data']->orangtua->pendidikan_ibu }}</td>
                            </tr>
                            <tr>
                                <td>Pekerjaan</td>
                                <td>: {{ $data['data']->orangtua->pekerjaan_ibu }}</td>
                            </tr>
                            <tr>
                                <td>Pengahasilan Bulanan</td>
                                <td>: {{ $data['data']->orangtua->penghasilan_ibu }}</td>
                            </tr>
                            <tr>
                                <td>No HP/WA</td>
                                <td>: {{ $data['data']->orangtua->telepon_ibu }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- wali --}}
        <div class="col-md-6 mb-3">
            <div class="card">
                <div class="card-header h4">
                    Data Wali
                </div>
                <div class="card-body pt-0">
                    <table>
                        <tbody>
                            <tr>
                                <td>Nama</td>
                                <td>: {{ $data['data']->orangtua->nama_wali }}</td>
                            </tr>
                            <tr>
                                <td>NIK</td>
                                <td>: {{ $data['data']->orangtua->nik_wali }}</td>
                            </tr>
                            <tr>
                                <td>Tahun Lahir</td>
                                <td>: {{ $data['data']->orangtua->tahun_lahir_wali }}</td>
                            </tr>
                            <tr>
                                <td>Jenjang Pendidikan</td>
                                <td>: {{ $data['data']->orangtua->pendidikan_wali }}</td>
                            </tr>
                            <tr>
                                <td>Pekerjaan</td>
                                <td>: {{ $data['data']->orangtua->pekerjaan_wali }}</td>
                            </tr>
                            <tr>
                                <td>Pengahasilan Bulanan</td>
                                <td>: {{ $data['data']->orangtua->penghasilan_wali }}</td>
                            </tr>
                            <tr>
                                <td>No HP/WA</td>
                                <td>: {{ $data['data']->orangtua->telepon_wali }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- prestasi --}}
        <div class="col-md-6 mb-3">
            <div class="card">
                <div class="card-header h4">
                    Data Prestasi
                </div>
                <div class="card-body pt-0">
                    <table>
                        <tbody>
                            <tr>
                                <td>Nama Prestasi</td>
                                <td>: {{ $data['data']->prestasi->nama_prestasi }}</td>
                            </tr>
                            <tr>
                                <td>Tingkat</td>
                                <td>: {{ $data['data']->prestasi->tingkat }}</td>
                            </tr>
                            <tr>
                                <td>Juara Ke</td>
                                <td>: {{ $data['data']->prestasi->juara }}</td>
                            </tr>
                            <tr>
                                <td>Tahun</td>
                                <td>: {{ $data['data']->prestasi->tahun }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- periodik --}}
        <div class="col-md-6 mb-3">
            <div class="card">
                <div class="card-header h4">
                    Data Periodik
                </div>
                <div class="card-body pt-0">
                    <table>
                        <tbody>
                            <tr>
                                <td>Tinggi Badan</td>
                                <td>: {{ $data['data']->periodik->tinggi_badan }} kg</td>
                            </tr>
                            <tr>
                                <td>Berat Badan</td>
                                <td>: {{ $data['data']->periodik->berat_badan }} cm</td>
                            </tr>
                            <tr>
                                <td>Lingkar Kepala</td>
                                <td>: {{ $data['data']->periodik->lingkar_kepala }} cm</td>
                            </tr>
                            <tr>
                                <td>Jarak Tempat Tinggal ke Sekolah</td>
                                <td>: {{ $data['data']->periodik->jarak }} m</td>
                            </tr>
                            <tr>
                                <td>Waktu Tempuh Berangkat ke Sekolah</td>
                                <td>: {{ $data['data']->periodik->waktu_tempuh }}  menit</td>
                            </tr>
                            <tr>
                                <td>Jumlah Saudara Kandung</td>
                                <td>: {{ $data['data']->periodik->jumlah_saudara }} Orang</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {{-- lampiran --}}
        <div class="col-md-6 mb-3">
            <div class="card">
                <div class="card-header h4">
                    Lampiran
                </div>
                <div class="card-body pt-0">
                    <table>
                        <tbody>
                            <tr>
                                <td>Akte Kelahiran</td>
                                <td>
                                    @if ($data['data']->lampiran->akte)
                                        : <a 
                                            href="{{ asset('storage/'.$data['data']->lampiran->akte) }}" 
                                            target="_blank">
                                            Lihat
                                        </a>
                                    @else
                                        :
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Kartu Keluarga</td>
                                <td>
                                    @if ($data['data']->lampiran->kk)
                                        : <a 
                                            href="{{ asset('storage/'.$data['data']->lampiran->kk) }}" 
                                            target="_blank">
                                            Lihat
                                        </a>
                                    @else
                                        :
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Badge (PTB, Mitra PTB dan Vidarta)</td>
                                <td>
                                    @if ($data['data']->lampiran->badge)
                                        : <a 
                                            href="{{ asset('storage/'.$data['data']->lampiran->badge) }}" 
                                            target="_blank">
                                            Lihat
                                        </a>
                                    @else
                                        :
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Pas Foto berwarna (terbaru)</td>
                                <td>
                                    @if ($data['data']->lampiran->foto)
                                        : <a 
                                            href="{{ asset('storage/'.$data['data']->lampiran->foto) }}" 
                                            target="_blank">
                                            Lihat
                                        </a>
                                    @else
                                        :
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Surat Keterangan Tanda Lulus SD (SKL)</td>
                                <td>
                                    @if ($data['data']->lampiran->skl)
                                        : <a 
                                            href="{{ asset('storage/'.$data['data']->lampiran->skl) }}" 
                                            target="_blank">
                                            Lihat
                                        </a>
                                    @else
                                        :
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
@endsection
