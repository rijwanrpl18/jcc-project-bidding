<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{{ $data['data']->nama }}</title>
    {{-- <link 
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" 
        rel="stylesheet" 
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" 
        crossorigin="anonymous"> --}}

    <style>
        .text-center {
            text-align: center;
        }
    </style>
</head>
<body>
    <table width="100%">
        <tbody>
            <tr>
                <td width="15%">
                    <img 
                        src="{{ public_path("assets/img/logo.png") }}" 
                        alt="" 
                        style="width: 100px"
                        class="">
                </td>
                <td width="70%" style="text-align: center">
                    <h4><b>SMP YPVDP BONTANG</b></h4>
                    <h4><b>PANITIA PENERIMAAN PESERAT DIDIK BARU (PPDB)</b></h4>
                    <h4><b>TAHUN PELAJARAN 2022-2023</b></h4>
                </td>
                <td width="15%"></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <img 
                        src="{{ $data['data']->lampiran->foto ? public_path('storage/'.$data['data']->lampiran->foto) : public_path('assets/img/profile.png') }}" 
                        alt=""
                        style="width: 200px; height: auto;">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <b>KATEGORI PENDAFTAR : {{ strtoupper($data['data']->profile != null ? ($data['master']['kategori_pendaftar'][$data['data']->profile->kategori] ?? '') : '') }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <b>PILIHAN PROGRAM : {{ strtoupper($data['data']->profile != null ? $data['master']['program'][$data['data']->profile->program] : '') }}</b>
                </td>
            </tr>
        </tbody>
    </table>
    {{-- <div class="row">
        <div class="col-sm-1 text-center">
            <img 
                src="{{ public_path("assets/img/logo.png") }}" 
                alt="" 
                style="width: 100px"
                class="">
        </div>
        <div class="col-sm-10 text-center">
            <h4><b>SMP YPVDP BONTANG</b></h4>
            <h4><b>PANITIA PENERIMAAN PESERAT DIDIK BARU (PPDB)</b></h4>
            <h4><b>TAHUN PELAJARAN 2022-2023</b></h4>
        </div>
    </div> --}}

    <p><b>DATA SISWA</b></p>
    <table width="100%" class="mt-0">
        <tr>
            <td width="35%">Nama</td>
            <td>: {{ $data['data']->nama }}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>: {{ $data['data']->profile != null ? ($data['data']->profile->jk ? 'Laki-Laki' : 'Perempuan') : '' }}</td>
        </tr>
        <tr>
            <td>NISN</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->nisn : '' }}</td>
        </tr>
        <tr>
            <td>Asal Sekolah</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->asal_sekolah : '' }}</td>
        </tr>
        <tr>
            <td>NIK</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->nik : '' }}</td>
        </tr>
        <tr>
            <td>Tempat Lahir</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->tempat_lahir : '' }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->tanggal_lahir : '' }}</td>
        </tr>
        <tr>
            <td>Agama</td>
            <td>: {{ $data['data']->profile != null ? $data['master']['agama'][$data['data']->profile->agama] : '' }}</td>
        </tr>
        <tr>
            <td>Berkebutuhan Khusus</td>
            <td>: {{ $data['data']->profile != null ? ($data['data']->profile->kebutuhan_khusus ? 'Ya' : 'Tidak') : '' }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->alamat : '' }}</td>
        </tr>
        <tr>
            <td>RT</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->rt : '' }}</td>
        </tr>
        <tr>
            <td>RW</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->rw : '' }}</td>
        </tr>
        <tr>
            <td>Kelurahan/Desa</td>
            <td>: {{ $data['desa'] != null ? $data['desa']->name : '' }}</td>
        </tr>
        <tr>
            <td>Kecamatan</td>
            <td>: {{ $data['desa'] != null ? $data['desa']->district->name : '' }}</td>
        </tr>
        <tr>
            <td>Kabupaten/Kota</td>
            <td>: {{ $data['desa'] != null ? $data['desa']->regency->name : '' }}</td>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>: {{ $data['desa'] != null ? $data['desa']->province->name : '' }}</td>
        </tr>
        <tr>
            <td>Kode Pos</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->kode_pos : '' }}</td>
        </tr>
        <tr>
            <td>Alat Transportasi ke Sekolah</td>
            <td>: {{ $data['data']->profile != null ? $data['master']['alat_transportasi'][$data['data']->profile->alat_transportasi] : '' }}</td>
        </tr>
        <tr>
            <td>Jenis Tinggal</td>
            <td>: {{ $data['data']->profile != null ? $data['master']['jenis_tinggal'][$data['data']->profile->jenis_tinggal] : '' }}</td>
        </tr>
        <tr>
            <td>No Telepon Rumah</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->telepon_rumah : '' }}</td>
        </tr>
        <tr>
            <td>No HP/WA Siswa</td>
            <td>: {{ $data['data']->profile != null ? $data['data']->profile->telepon : '' }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>: {{ $data['data']->email }}</td>
        </tr>
    </table>

    <p class="mt-3 mb-0"><b>DATA AYAH KANDUNG</b></p>
    <table width="100%" class="mt-0">
        <tr>
            <td width="35%">Nama</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->nama_ayah : '' }}</td>
        </tr>
        <tr>
            <td>NIK</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->nik_ayah : '' }}</td>
        </tr>
        <tr>
            <td>Tahun Lahir</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->tahun_lahir_ayah : '' }}</td>
        </tr>
        <tr>
            <td>Jenjang Pendidikan</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->pendidikan_ayah : '' }}</td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->pekerjaan_ayah : '' }}</td>
        </tr>
        <tr>
            <td>Pengahasilan Bulanan</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->penghasilan_ayah : '' }}</td>
        </tr>
        <tr>
            <td>No HP/WA</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->telepon_ayah : '' }}</td>
        </tr>
    </table>

    <p class="mt-3 mb-0"><b>DATA IBU KANDUNG</b></p>
    <table width="100%" class="mt-0">
        <tr>
            <td width="35%">Nama</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->nama_ibu : '' }}</td>
        </tr>
        <tr>
            <td>NIK</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->nik_ibu : '' }}</td>
        </tr>
        <tr>
            <td>Tahun Lahir</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->tahun_lahir_ibu : '' }}</td>
        </tr>
        <tr>
            <td>Jenjang Pendidikan</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->pendidikan_ibu : '' }}</td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->pekerjaan_ibu : '' }}</td>
        </tr>
        <tr>
            <td>Pengahasilan Bulanan</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->penghasilan_ibu : '' }}</td>
        </tr>
        <tr>
            <td>No HP/WA</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->telepon_ibu : '' }}</td>
        </tr>
    </table>

    <p class="mt-3 mb-0"><b>DATA WALI</b></p>
    <table width="100%" class="mt-0">
        <tr>
            <td width="35%">Nama</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->nama_wali : '' }}</td>
        </tr>
        <tr>
            <td>NIK</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->nik_wali : '' }}</td>
        </tr>
        <tr>
            <td>Tahun Lahir</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->tahun_lahir_wali : '' }}</td>
        </tr>
        <tr>
            <td>Jenjang Pendidikan</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->pendidikan_wali : '' }}</td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->pekerjaan_wali : '' }}</td>
        </tr>
        <tr>
            <td>Pengahasilan Bulanan</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->penghasilan_wali : '' }}</td>
        </tr>
        <tr>
            <td>No HP/WA</td>
            <td>: {{ $data['data']->orangtua != null ? $data['data']->orangtua->telepon_wali : '' }}</td>
        </tr>
    </table>

    <p class="mt-3 mb-0"><b>DATA PERIODIK</b></p>
    <table width="100%" class="mt-0">
        <tr>
            <td width="35%">Tinggi Badan</td>
            <td>: {{ $data['data']->periodik != null ? $data['data']->periodik->tinggi_badan : 0 }} kg</td>
        </tr>
        <tr>
            <td>Berat Badan</td>
            <td>: {{ $data['data']->periodik != null ? $data['data']->periodik->berat_badan : 0 }} cm</td>
        </tr>
        <tr>
            <td>Lingkar Kepala</td>
            <td>: {{ $data['data']->periodik != null ? $data['data']->periodik->lingkar_kepala : 0 }} cm</td>
        </tr>
        <tr>
            <td>Jarak Tempat Tinggal ke Sekolah</td>
            <td>: {{ $data['data']->periodik != null ? $data['data']->periodik->jarak : 0 }} m</td>
        </tr>
        <tr>
            <td>Waktu Tempuh Berangkat ke Sekolah</td>
            <td>: {{ $data['data']->periodik != null ? $data['data']->periodik->waktu_tempuh : 0 }}  menit</td>
        </tr>
        <tr>
            <td>Jumlah Saudara Kandung</td>
            <td>: {{ $data['data']->periodik != null ? $data['data']->periodik->jumlah_saudara : 0 }} Orang</td>
        </tr>
    </table>
    
    <p class="mt-3 mb-0"><b>DATA PRESTASI</b></p>
    <table width="100%" class="mt-0">
        <tr>
            <td width="35%">Nama Prestasi</td>
            <td>: {{ $data['data']->prestasi != null ? $data['data']->prestasi->nama_prestasi : '' }}</td>
        </tr>
        <tr>
            <td>Tingkat</td>
            <td>: {{ $data['data']->prestasi != null ? $data['data']->prestasi->tingkat : '' }}</td>
        </tr>
        <tr>
            <td>Juara Ke</td>
            <td>: {{ $data['data']->prestasi != null ? $data['data']->prestasi->juara : '' }}</td>
        </tr>
        <tr>
            <td>Tahun</td>
            <td>: {{ $data['data']->prestasi != null ? $data['data']->prestasi->tahun : '' }}</td>
        </tr>
    </table>
</body>
</html>
