<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta 
        name="viewport" 
        content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta 
        name="csrf-token" 
        content="{{ csrf_token() }}">

    <title>
        Penerimaan Siswa Baru
    </title>

    <!-- Icon fonts -->
    <link 
        rel="stylesheet" 
        href="{{ asset('assets/vendor/fonts/fontawesome.css') }}">

    <!-- Core stylesheets -->
    <link 
        href="{{ asset('css/app.css') }}" 
        rel="stylesheet">
    <link 
        rel="stylesheet" 
        href="{{ asset('assets/vendor/css/rtl/appwork.css') }}" 
        class="theme-settings-appwork-css">
    <link 
        rel="stylesheet" 
        href="{{ asset('assets/vendor/css/rtl/uikit.css') }}">

    <!-- Page -->
    <link 
        rel="stylesheet" 
        href="{{ asset('assets/vendor/css/pages/authentication.css') }}">
</head>
<body>
    <div class="page-loader">
        <div class="bg-primary"></div>
    </div>
    <div class="authentication-wrapper authentication-3">
        <div class="authentication-inner">
            <div class="d-flex col-lg-5 align-items-center bg-white p-5">
                <div class="d-flex col-sm-8 col-md-6 col-lg-12 px-0 px-xl-4 mx-auto">
                    <div class="w-100">
                        <div class="d-flex justify-content-center align-items-center">
                            <div class="ui-w-100">
                                <div 
                                    class="w-100 position-relative" 
                                    style="padding-bottom: 54%; height: 100px; height: 100px;">
                                    <img 
                                        src="{{ asset('assets/img/logo.png') }}" 
                                        alt="" 
                                        class="w-100 h-100 position-absolute">
                                </div>
                            </div>
                        </div>
                        
                        <h4 class="text-center font-weight-normal mt-5 mb-0">Daftar</h4>

                        @if (Session::get('success'))
                            <div class="alert alert-dark-success alert-dismissible fade show">
                                <i class="fa fa-check"></i>
                                {!! session('success') !!}
                            </div>
                        @endif
                        
                        <form 
                            class="my-3" 
                            method="POST" 
                            action="{{ route('register') }}">
                            @csrf
    
                            <div class="form-group">
                                <label class="form-label">Nama</label>
                                <input 
                                    type="text" 
                                    class="form-control"
                                    placeholder="Nama" 
                                    name="nama" 
                                    value="{{ old('nama') ?? '' }}"
                                    required
                                    autofocus>
                                    {!! $errors->first('nama', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
    
                            <div class="form-group">
                                <label class="form-label">Alamat</label>
                                <textarea 
                                    name="alamat" 
                                    id="alamat" 
                                    cols="30"
                                    class="form-control" 
                                    placeholder="Alamat"
                                    required
                                    rows="3">{{ old('alamat') ?? '' }}</textarea>
                                {!! $errors->first('alamat', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
    
                            <div class="form-group">
                                <label class="form-label">Asal Sekolah</label>
                                <input 
                                    type="text" 
                                    class="form-control"
                                    placeholder="Asal Sekolah" 
                                    value="{{ old('asal_sekolah') ?? '' }}"
                                    required
                                    name="asal_sekolah">
                                {!! $errors->first('asal_sekolah', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
    
                            <div class="form-group">
                                <label class="form-label">No Hp</label>
                                <input 
                                    type="text" 
                                    class="form-control"
                                    placeholder="No Hp" 
                                    value="{{ old('no_hp') ?? '' }}"
                                    required
                                    name="no_hp">
                                {!! $errors->first('no_hp', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
    
                            <div class="form-group">
                                <label class="form-label">Email</label>
                                <input 
                                    type="email" 
                                    class="form-control"
                                    placeholder="Email" 
                                    value="{{ old('email') ?? '' }}"
                                    required
                                    name="email">
                                {!! $errors->first('email', '<small class="form-text text-danger">:message</small>') !!}
                            </div>
    
                            <div class="form-group">
                                <label class="form-label">Pilihan Program</label>
                                <select 
                                    name="program" 
                                    class="form-control"
                                    id="program"
                                    required>
                                    <option value="null">Pilih</option>
                                    <option value="1" {{ old('program') == 1 ? 'selected' :'' }}>Kelas Tahfidz</option>
                                    <option value="2" {{ old('program') == 2 ? 'selected' :'' }}>Kelas IT</option>
                                    <option value="3" {{ old('program') == 3 ? 'selected' :'' }}>Reguler</option>
                                </select>
                                {!! $errors->first('program', '<small class="form-text text-danger">:message</small>') !!}
                            </div>

                            <div 
                                class="form-group" 
                                style="position: relative;">
                                <label class="form-label d-flex justify-content-between align-items-end">
                                    <div>Password</div>
                                </label>
                                <input 
                                    type="password" 
                                    class="form-control" 
                                    name="password" 
                                    placeholder="password" 
                                    required
                                    id="password">
                                <div 
                                    style="position: absolute; right: 5px; top: 50%; transform: translate(-50%,0); cursor: pointer;" 
                                    id="togglePass">
                                    <i class="fa fa-eye" id="icon-pass"></i>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <button 
                                    type="submit" 
                                    class="btn btn-primary">
                                    Daftar
                                </button>
                            </div>
                        </form>
                        <div class="d-flex justify-content-between">
                            <b class="mt-2">Sudah punya akun?</b>
                            <a 
                                href="{{ route('login') }}"
                                class="btn btn-success">
                                Login
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div 
                class="d-none d-lg-flex col-lg-8 align-items-center ui-bg-cover ui-bg-overlay-container p-5" 
                style="background-image: url('assets/img/bg/21.jpg');">
          </div>
        </div>
    </div>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/toggle-pass.js') }}"></script>
</body>
</html>
