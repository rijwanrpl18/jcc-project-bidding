@extends('layouts.application')
@section('module', 'Data Siswa')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header h4 text-center">Data Siswa</div>
            <div class="table-responsive">
                <table class="table card-table">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>NISN</th>
                            <th>Email</th>
                            <th>Jenis Kelamin</th>
                            <th>Asal Sekolah</th>
                            <th>No Telepon</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i = ($data['data']->currentPage() - 1) * $data['data']->perPage();
                        @endphp
                        @foreach ($data['data'] as $value)
                            @php
                                $i++;
                            @endphp
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $value->nama }}</td>
                                <td>{{ $value->nisn }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->jk ? 'Laki-Laki' : 'Perempuan' }}</td>
                                <td>{{ $value->asal_sekolah }}</td>
                                <td>{{ $value->telepon }}</td>
                                <td class="text-center">
                                    <a 
                                        href="{{ route('siswa.show', ['id' => $value->user_id]) }}" 
                                        class="btn icon-btn btn-primary btn-sm" 
                                        data-toggle="tooltip" 
                                        data-placement="top" 
                                        data-state="dark" 
                                        title="Detail Siswa">
                                        <i class="fa fa-user"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-3">
                        Total Record : <strong>{{$data['data']->count() + ($data['limit']*($data['data']->currentPage() - 1))}}</strong> of <strong>{{$data['data']->total()}}</strong>
                    </div>
                    <div class="col-md-9">
                        {{ $data['data']->appends(request()->input())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
